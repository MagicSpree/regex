package com.company.bootstrap;

import com.company.Regex;

import java.util.List;

public class Bootstrap {

    final String REGEX_DOUBLE = "^[-+]?[0-9]*[.,]?[0-9]+(?:[eE][-+]?[0-9]+)?$";
    final String REGEX_PREFIX = "^[пП][рР]([иИ]|[еЕ]).*";
    final String REGEX_IP = "((0|1\\d{0,2}|2([0-4][0-9]|5[0-5]))\\.){3}(0|1\\d{0,2}|2([0-4][0-9]|5[0-5]))";
    final String REGEX_PARENTHESIS = "^([^()]*\\((?:[^()])*\\)[^()]*)+$|^[^()]+?$";


    public void run() {
        checkRegExDouble(); // Проверка на числа с плавающей запятой
        checkRegExPrefix(); // Проверка на слова начинающиеся с приставкой ПРЕ- ПРИ-
        checkRegExIP();     // Проверка на формат IP-адреса
        checkRegExParenthesis(); // Проверка на правильность поставленных скобок
    }

    /**
     * Проверка на правильность поставленных скобок
     */
    private void checkRegExParenthesis() {
        List<String> elementsWithParenthesis= List.of("(3x+1)+2", "(3x+1))+2", "3x+1");
        getResultRegularExpression(REGEX_PARENTHESIS, elementsWithParenthesis);
    }

    /**
     * Проверка на формат IP-адреса
     */
    private void checkRegExIP() {
        List<String> ipElements = List.of("127.0.0.1", "255.255.255.0", "255.255.00.0", "00.6.7.888", "abc.def.gha.bcd", "123.ff.12.DD", "14ff.84k.255.255");
        getResultRegularExpression(REGEX_IP, ipElements);
    }

    /**
     * Проверка на слова начинающиеся с приставкой ПРЕ- и ПРИ-
     */
    private void checkRegExPrefix() {
        List<String> words = List.of("прИмер", "перелет", "пРивет", "предложение", "Предмет", "Прикол");
        getResultRegularExpression(REGEX_PREFIX, words);
    }

    /**
     * Проверка на числа с плавающей запятой
     */
    private void checkRegExDouble() {
        List<String> doubleElements = List.of("22.13e+10", "-4.e10", "33.11", "44,12e10", "0.3float", "Dinar");
        getResultRegularExpression(REGEX_DOUBLE, doubleElements);
    }

    /**
     * Вывод елементов, выполняющих условие регулярного выражения
     */
    private void getResultRegularExpression(String regEx, List<String> sourceString)
    {
        Regex.check(regEx, sourceString)
             .forEach(System.out::println);
    }

}
