package com.company;

import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Collectors;

public class Regex {

    Pattern pattern;

    public Regex(String regex) {
        this.pattern = Pattern.compile(regex);
    }

    public List<String> check(List<String> list) {
        return list.stream().filter(el -> pattern.matcher(el).matches()).collect(Collectors.toList());
    }

    /**
     * Проверка елементов с помощью регулярного выражения
     */
    public static List<String> check(String regex, List<String> list) {
        try {
            Pattern pattern = Pattern.compile(regex);
            return list.stream()
                    .filter(el -> pattern.matcher(el).matches())
                    .collect(Collectors.toList());
        } catch (PatternSyntaxException patternSyntaxException) {
            System.out.println(patternSyntaxException.getMessage());
            //лучше logger.error(patternSyntaxException.getMessage());

        }
        return Collections.emptyList();
    }

}
